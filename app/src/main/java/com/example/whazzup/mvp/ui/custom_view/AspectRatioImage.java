package com.example.whazzup.mvp.ui.custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.applications.whazzup.mvpapplication.R;

/**
 * Created by Alex on 29.10.2016.
 */

public class AspectRatioImage extends ImageView {
    private static final float DEFAULT_ASPECT_RATIO = 1.17f;
    private final float mAspectRatio;

    public AspectRatioImage(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int newWith;
        int newHeight;

        newHeight = getMeasuredHeight();
        newWith = (int) (newHeight / mAspectRatio);

        /*
        newWith = getMeasuredWidth();
        newHeight = (int) (newWith/mAspectRatio);
*/
        setMeasuredDimension(newWith, newHeight);  //Значения устанавливаются во View
    }
}
