package com.example.whazzup.mvp.mvp.presenters;

/**
 * Created by Alex on 29.10.2016.
 */

public interface ICatalogPresenter {
    void clickonBuyButton(int positin);
    boolean checkUserAuth();
}
