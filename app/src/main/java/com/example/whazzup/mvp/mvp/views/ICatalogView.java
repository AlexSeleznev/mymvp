package com.example.whazzup.mvp.mvp.views;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by Alex on 29.10.2016.
 */

public interface ICatalogView extends IView {
    void showAddToCartMessage (ProductDto product);
    void showCatalogView(List<ProductDto> productList);
    void showAuthScreen();
    void updateProductCounter();
}
