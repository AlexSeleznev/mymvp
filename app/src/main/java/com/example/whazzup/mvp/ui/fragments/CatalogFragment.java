package com.example.whazzup.mvp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.applications.whazzup.mvpapplication.R;
import com.example.whazzup.mvp.data.storage.dto.ProductDto;
import com.example.whazzup.mvp.mvp.presenters.CatalogPresenter;
import com.example.whazzup.mvp.mvp.views.ICatalogView;
import com.example.whazzup.mvp.ui.activities.RootActivity;
import com.example.whazzup.mvp.ui.fragments.adapters.CatalogAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;


public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    private CatalogPresenter mCatalogPresenter;

    @BindView(R.id.add_to_cart_btn)
    Button mAddToCartBtn;

    @BindView(R.id.product_pager)
    ViewPager mProductPager;

    @BindView(R.id.indicator)
    CircleIndicator mIndicator;


    public CatalogFragment() {
        mCatalogPresenter = CatalogPresenter.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        mCatalogPresenter.takeView(this);
        mCatalogPresenter.initView();
        mAddToCartBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mCatalogPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.add_to_cart_btn){
            mCatalogPresenter.clickonBuyButton(mProductPager.getCurrentItem());
        }
    }


    //region =====================Iview=======================

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
    //endregion ===============================================

    //region =====================ICtatlogView=======================
    @Override
    public void showAddToCartMessage(ProductDto product) {

    }

    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for(ProductDto product : productList){
            adapter.addItem(product);
        }

        mProductPager.setAdapter(adapter);
        mIndicator.setViewPager(mProductPager);
    }

    @Override
    public void showAuthScreen() {
        // TODO: 29.10.2016 show Auth Screen if user not auth
    }

    @Override
    public void updateProductCounter() {
        // TODO: 29.10.2016 update count product on cart icon 
    }

    //endregion ===============================================

    private RootActivity getRootActivity(){
        return (RootActivity) getActivity();
    }


}


