package com.example.whazzup.mvp.util;

/**
 * Created by Alex on 22.10.2016.
 */

public interface ConstantManager {
    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
    String PRODUCT_KEY = "PRODUCT_KEY";
}
