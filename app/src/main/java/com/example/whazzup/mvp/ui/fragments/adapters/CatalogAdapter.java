package com.example.whazzup.mvp.ui.fragments.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;
import com.example.whazzup.mvp.ui.fragments.ProductFragment;

import java.util.ArrayList;
import java.util.List;


public class CatalogAdapter extends FragmentPagerAdapter {

    private List<ProductDto> productList = new ArrayList<>();

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.getInstance(productList.get(position));
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    public void addItem(ProductDto product){
        productList.add(product);
        notifyDataSetChanged();
    }
}
