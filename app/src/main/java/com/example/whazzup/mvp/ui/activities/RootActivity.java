package com.example.whazzup.mvp.ui.activities;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.applications.whazzup.mvpapplication.BuildConfig;
import com.applications.whazzup.mvpapplication.R;
import com.example.whazzup.mvp.data.managers.DataManager;
import com.example.whazzup.mvp.mvp.views.IView;
import com.example.whazzup.mvp.ui.fragments.CatalogFragment;
import com.example.whazzup.mvp.ui.fragments.ProductFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RootActivity extends AppCompatActivity implements IView, NavigationView.OnNavigationItemSelectedListener {

    private ProgressDialog mProgressDialog;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorContainer;

    @BindView(R.id.fragment_container)
    FrameLayout mFrameContainer;

    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this, R.layout.dialog_splash);
        mFragmentManager = getSupportFragmentManager();
        initToolbar();
        initNavigationDrawer();

        if(savedInstanceState == null){
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment(), "catalog")
                    .commit();
        }

    }

    private void initNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawerlayoup_open_title, R.string.drawerlayout_close_title);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.catalog_drawer:
                fragment = new CatalogFragment();

                break;
            case R.id.favorite_drawer:
                break;
            case R.id.basket_drawer:
                break;
            case R.id.notification_drawer:
                break;
            case R.id.profile_drawer:
                break;
        }
        if(fragment!=null){
            mFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack("catalog").commit();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(mFragmentManager.getBackStackEntryCount() > 1){
            super.onBackPressed();
        } else {
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
                showAlertDialog();
        }


    }

    //region =====================IView=======================
    public void showMessage(String message){
        Snackbar.make(mCoordinatorContainer,message,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e){
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }else{
            showMessage("Извините что то пошло нетак, попробуйте пойзже");
        }

    }

    @Override
    public void showLoad(){
        // TODO: 20.10.2016 Показать загрузчик
        if(mProgressDialog==null){
            mProgressDialog=new ProgressDialog(this,R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_splash);
        }
        else{
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_splash);
        }
    }

    @Override
    public void hideLoad(){
        // TODO: 20.10.2016 Скрыть загрузчик
        mProgressDialog.hide();

    }

    //endregion ===============================================

    private void showAlertDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Вы действительно хотите выйти из приложения?");
        builder.setCancelable(true);
        builder.setNeutralButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finishAffinity();
            }
        });
        builder.show();
    }


}
