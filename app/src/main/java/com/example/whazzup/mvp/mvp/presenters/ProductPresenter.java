package com.example.whazzup.mvp.mvp.presenters;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;
import com.example.whazzup.mvp.mvp.models.ProductModel;
import com.example.whazzup.mvp.mvp.views.IProductView;

/**
 * Created by Alex on 28.10.2016.
 */

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {

    private ProductModel mProductModel;
    private ProductDto mProduct;
    public static ProductPresenter getInstance(ProductDto product){
        return new ProductPresenter(product);
    }

    private ProductPresenter(ProductDto productDto) {
        mProductModel = new ProductModel();
        mProduct = productDto;
    }


    @Override
    public void initView() {
        if(getView()!=null){
            getView().showProductView(mProduct);
        }
    }

    @Override
    public void clickOnPlus() {
        if(mProduct.getCount()>0){
            mProduct.addProduct();
            mProductModel.updateProduct(mProduct);
            if(getView()!=null){
                getView().updateProducCountView(mProduct);
            }
        }
    }

    @Override
    public void clickOnMinus() {
        if(mProduct.getCount()>0){
           mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if(getView()!=null){
                getView().updateProducCountView(mProduct);
            }
        }
    }
}
