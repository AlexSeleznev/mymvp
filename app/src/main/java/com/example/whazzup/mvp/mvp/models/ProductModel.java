package com.example.whazzup.mvp.mvp.models;

import com.example.whazzup.mvp.data.managers.DataManager;
import com.example.whazzup.mvp.data.storage.dto.ProductDto;

/**
 * Created by Alex on 28.10.2016.
 */

public class ProductModel {
    DataManager mDataManager = DataManager.getInstance();

    // TODO: 28.10.2016 get product from datamanager
    public ProductDto getProdutById (int productId){
       return mDataManager.getProductById(productId);
    }

public void updateProduct(ProductDto product){
    mDataManager.updateProduct();
}



}
