package com.example.whazzup.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.applications.whazzup.mvpapplication.R;

public class RootActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

    }
}
