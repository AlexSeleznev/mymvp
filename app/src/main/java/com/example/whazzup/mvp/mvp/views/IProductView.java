package com.example.whazzup.mvp.mvp.views;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;

/**
 * Created by Alex on 28.10.2016.
 */

public interface IProductView extends IView {
    void showProductView(ProductDto product);
    void updateProducCountView(ProductDto product);
}
