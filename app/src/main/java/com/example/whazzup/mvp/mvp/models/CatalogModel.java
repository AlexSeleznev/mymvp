package com.example.whazzup.mvp.mvp.models;

import com.example.whazzup.mvp.data.managers.DataManager;
import com.example.whazzup.mvp.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by Alex on 29.10.2016.
 */

public class CatalogModel {
    DataManager mDataManager = DataManager.getInstance();

    public CatalogModel() {
    }

    public List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.isAuthUser();
    }


}
