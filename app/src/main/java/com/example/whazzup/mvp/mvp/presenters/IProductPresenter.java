package com.example.whazzup.mvp.mvp.presenters;

/**
 * Created by Alex on 28.10.2016.
 */

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}
