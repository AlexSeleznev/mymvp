package com.example.whazzup.mvp.mvp.presenters;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;
import com.example.whazzup.mvp.mvp.models.CatalogModel;
import com.example.whazzup.mvp.mvp.views.ICatalogView;

import java.util.List;

/**
 * Created by Alex on 29.10.2016.
 */
public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    private static CatalogPresenter ourInstance = new CatalogPresenter();
    private CatalogModel mCatalogModel;
    private List<ProductDto> productList;

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mCatalogModel = new CatalogModel();

    }

    @Override
    public void initView() {
        if(productList==null){
            productList = mCatalogModel.getProductList();
        }

        if(getView()!=null){
            getView().showCatalogView(productList);
        }
    }

    @Override
    public void clickonBuyButton(int positin) {
        if(getView()!=null){
            if(checkUserAuth()){
                getView().showAddToCartMessage(productList.get(positin));
            }else{
                getView().showAuthScreen();
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
         return mCatalogModel.isUserAuth();
    }
}
