package com.example.whazzup.mvp.mvp.presenters;

import com.example.whazzup.mvp.data.storage.dto.ProductDto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 28.10.2016.
 */

public class ProductPresenterFactory {
    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<>();

    private static void registerPresenter(ProductDto productDto, ProductPresenter productPresenter){
        sPresenterMap.put(String.valueOf(productDto.getId()), productPresenter);
    }

   public static ProductPresenter getInstance(ProductDto productDto){
        ProductPresenter presenter = sPresenterMap.get(productDto.getId());
        if(presenter==null){
            presenter = ProductPresenter.getInstance(productDto);
            registerPresenter(productDto, presenter);
        }
        return presenter;
    }
}
