package com.example.whazzup.mvp.mvp.views;

/**
 * Created by Alex on 28.10.2016.
 */

public interface IView {
    void showMessage(String message);
    void showError(Throwable e);
    void showLoad();
    void hideLoad();

}
