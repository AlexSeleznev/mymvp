package com.example.whazzup.mvp.mvp.views;

import android.support.annotation.Nullable;

import com.example.whazzup.mvp.mvp.presenters.IAuthPresenter;
import com.example.whazzup.mvp.ui.custom_view.AuthPanel;

/**
 * Created by Alex on 20.10.2016.
 */

public interface IAuthView extends IView {


    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    //void testShowLoginCard();
    @Nullable
    AuthPanel getAuthPanel();

    void showCatalogScreen();
}
