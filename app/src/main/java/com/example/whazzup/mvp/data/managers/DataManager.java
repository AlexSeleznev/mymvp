package com.example.whazzup.mvp.data.managers;


import com.example.whazzup.mvp.data.storage.dto.ProductDto;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    private static DataManager ourInstance = new DataManager();
    private List<ProductDto> mMockProductList;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager(){
        generateMockData();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 28.10.2016 this temp sample mock data fix me (may be load from DB)
        return mMockProductList.get(productId+1);
    }

    public void updateProduct() {
        // TODO: 28.10.2016 update product count or status (somethig in product) save in Db

    }

    public List<ProductDto> getProductList(){
        // TODO: 28.10.2016 load productlist from anywhere
        return mMockProductList;
    }

    private void generateMockData(){
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "BARUM Bravuris 2 195/50R16","http://218.pneumatico.ru/tyres/img/350/bravuris-2.jpg","Летняя шина BARUM Bravuris 2",4596,1));
        mMockProductList.add(new ProductDto(2, "BFGOODRICH Activan 195/65R16", "http://218.pneumatico.ru/tyres/img/350/activan.jpg", "Летняя шина BFGOODRICH Activan", 5464, 1));
        mMockProductList.add(new ProductDto(3, "BRIDGESTONE Dueler A/T 697 215/70R16", "http://218.pneumatico.ru/tyres/img/350/dueler-a-t-697.jpg", "Летняя шина BRIDGESTONE Dueler A/T 697", 6231, 1));

    }

    public boolean isAuthUser() {
        // TODO: 29.10.2016 Check userAuthtoken in sharedPreferences
        return false;
    }
}
