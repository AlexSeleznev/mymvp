package com.example.whazzup.mvp.ui.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.applications.whazzup.mvpapplication.R;
import com.example.whazzup.mvp.data.storage.dto.ProductDto;
import com.example.whazzup.mvp.mvp.presenters.ProductPresenter;
import com.example.whazzup.mvp.mvp.presenters.ProductPresenterFactory;
import com.example.whazzup.mvp.mvp.views.IProductView;
import com.example.whazzup.mvp.ui.activities.RootActivity;
import com.example.whazzup.mvp.util.ConstantManager;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 28.10.2016.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {

    @BindView(R.id.product_name_txt)
    TextView mProductName;

    @BindView(R.id.product_description_txt)
    TextView mProductDescription;

    @BindView(R.id.product_image_img)
    ImageView mProdusctImage;

    @BindView(R.id.product_count_txt)
    TextView mProductCount;

    @BindView(R.id.product_price_txt)
    TextView mProductPrice;

    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;

    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @BindDrawable(R.drawable.emp_image)
    Drawable productDraw;

    private ProductPresenter mPresenter;
    private Context mContext;

    public ProductFragment() {
    }

    public static ProductFragment getInstance(ProductDto product){
        Bundle bundle = new Bundle();
        bundle.putParcelable(ConstantManager.PRODUCT_KEY, product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle){
        if(bundle!=null){
            ProductDto product = bundle.getParcelable(ConstantManager.PRODUCT_KEY);
            mPresenter = ProductPresenterFactory.getInstance(product);

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //region =====================IProductView=======================

    @Override
    public void showProductView(ProductDto product) {
        mProductName.setText(product.getProductName());
        mProductDescription.setText(product.getDescription());
        mProductCount.setText(String.valueOf(product.getCount()));
        if(product.getCount()>0){
            mProductPrice.setText(String.valueOf(product.getCount() * product.getPrice()) + ".-");
        }else{
            mProductPrice.setText(String.valueOf(product.getPrice()));
        }
        Picasso.with(getActivity()).load(product.getImageUrl()).into(mProdusctImage);

    }

    @Override
    public void updateProducCountView(ProductDto product) {
        mProductCount.setText(String.valueOf(product.getCount()));
        if(product.getCount()>0){
            mProductPrice.setText(product.getCount()*product.getPrice() + ".-");
        }

    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
    //endregion ===============================================

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }

    }


}
